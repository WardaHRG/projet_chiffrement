
#include "head.h"

int main()
{

    FILE* file=NULL;
    file=fopen("source.txt", "rt");



    lire_source(file);
    saisir_perroquet();
    chiffrement();
    dechiffrement();

    //instructions pour supprimer la source
    if(dechiffrement()==1)
    {
        printf("\nLe chifrement a ete realise avec succes.\nVoulez-vous supprimer la source ? O/N");
        char rep;
        scanf("%c", &rep);
        switch(rep)
        {
            case'O':
            case'o':
                remove("source.txt");
                printf("\nFICHIER SOURCE SUPPRIME");
                break;
            default:
                printf("\nFICHIER SOURCE NON SUPPRIME");
                break;
        }

    }

    return 0;
}
