#include "head.h"

#define LG 3

// VARIABLES  GLOBALES
char tableau_source[500];
char tableau_chiffr[500];
char tableau_dechiffr[500];
int taille_tab_source=0;
int taille_tab_perroq=0;
int taille_tab_chiffr=0;
char pqt[20+1];



void lire_source(FILE* file)
{
    //COMMENTAIRE ERGONOMIE VISUELLE
    printf("\nSTART READING SOURCE\n");

    FILE* fp = file;

    //OUVERTURE
    fp = fopen("source.txt", "rt");
    if (fp == NULL)
    {
        printf("\n! FAIL OPEN FILE SOURCE !");
        return EXIT_FAILURE;
    }
    else
        printf("\n\n\n OPEN FILE SOURCE OK");


    //TEST FICHIER VIDE
    if (feof(fp))
    {
        printf("\n! EMPTY FILE SOURCE !");
    }

    //#####
    //INSTRUCTION PRINCIPALE : lire fichier source caractere par caractere et affecter chaque valeur dans une case du tableau cree
    //#####

    char s;
    fread(&s, sizeof(char), 1, fp);
    int i=0;
    while(!feof(fp))
    {
        tableau_source[i]=s;

        fread(&s, sizeof(char), 1, fp);



        if (feof(fp))
        {
            printf("\nEND FILE SOURCE\n");
        }

        i++;

    }

    taille_tab_source=i;


    //FERMETURE
    int ret = fclose(fp);
    if (ret != 0)
    {
        printf("\n! FAIL CLOSE FILE SOURCE!");
        return EXIT_FAILURE;
    }
    else
        printf("\nCLOSE FILE SOURCE OK\n\n\n");

    //COMMENTAIRE ERGONOMIE VISUELLE
    printf("\nEND STEP 1\nBEGIN STEP 2 : PERROQUET\n\n");


}

void saisir_perroquet()
{
    //SAISIE DU PERROQUET PAR L'USER

    printf("\nSaisir le perroquet defini (limite a 20 caracteres) :\n");

    gets(pqt);
    printf("\nPerroquet saisi : %s\n", pqt);




    //CREATION/OUVERTURE FICHIER PERROQ.DEF

    FILE* fperr = NULL;
    fperr = fopen("perroq.def", "w+t");
    if (fperr == NULL)
    {
        printf("\n! WARNING : FAIL CREATE FILE perroq.def !\n");
        return EXIT_FAILURE;
    }
    else
        printf("\n\n\nOPEN FILE perroq.def OK\n");



    //#####
    //COPIE DU PERROQUET SAISI DANS PERROQ.DEF
    //#####


    //COMMENTAIRE ERGONOMIE VISUELLE
    printf("\nBEGIN STEP 3 : COPY PERROQUET IN perroq.def\n");

    int i=0;
    char* ptr=NULL;
    while(i<strlen(pqt))
    {

        ptr=&pqt[i];

        fwrite(ptr, sizeof(char), 1, fperr);

        i++;
    }

    //FERMETURE
    int ret = fclose(fperr);
    if (ret != 0)
    {
        printf("\n! WARNING : FAIL CLOSE FILE PERROQ!");
        return EXIT_FAILURE;
    }
    else
        printf("\nCLOSE FILE PERROQ OK\n\n\n");

    //COMMENTAIRE ERGONOMIE VISUELLE
    printf("\nEND STEP 3\nBEGIN STEP 4 : ENCRYPTION");
}



void chiffrement()
{

    //OUVERTURE/CREATION FICHIER RESULTAT
    FILE* fp7=NULL;
    fp7 = fopen("dest.crt", "w+t");
    if (fp7 == NULL)
    {
        printf("\n! WARNING : FAIL OPEN FILE dest.crt !");
        return EXIT_FAILURE;
    }
    else
        printf("\nOPEN FILE dest.crt OK");


    //#####
    //INSTRUCTION PRINCIPALE : calcul des chiffrements + ecriture dans fichier resultat + remplissage tab_cryptage
    //#####

    int i, j;
    j = 0;
    char* ptr2=NULL;

    for(i=0; i<taille_tab_source; i++)
    {

        tableau_chiffr[i] = tableau_source[i] - pqt[j];

        ptr2 = &tableau_chiffr[i];
        fwrite(ptr2, sizeof(char), 1, fp7);

        j++;
        if(j>=strlen(pqt))
            j=0; //remet j a 0 pour recommencer le perroquet

    }


    taille_tab_chiffr = i;



    //FERMETURE
    int ret = fclose(fp7);
    if (ret != 0)
    {
        printf("\n! FAIL CLOSE FILE dest.crt !\n");
        return EXIT_FAILURE;
    }
    else
        printf("\nCLOSE FILE dest.crt OK\n");

    printf("\nENCRYPTION : SUCCESS\n");

    //COMMENTAIRE ERGONOMIE VISUELLE
    printf("\nEND STEP 4\n\n\n");


}


int dechiffrement()
{
    //COMMENTAIRE ERGONOMIE VISUELLE
    printf("\nBEGIN STEP 5 : DECRYPTING dest.crt\n");

    //OUVERTURE FICHIER RESULTAT
    FILE* fp=NULL;
    fp = fopen("dest.crt", "rt");
    if (fp == NULL)
    {
        printf("\n! FAIL OPEN FILE dest.crt !");
        return EXIT_FAILURE;
    }
    else
        printf("\nOPEN FILE dest.crt OK\n");




    //OUVERTURE/CREATION FICHIER DECHIFFREMENT
    FILE* fp3=NULL;
    fp3 = fopen("dechiffr.crt", "w+t");
    if (fp3 == NULL)
    {
        printf("\n! FAIL OPEN FILE dechiffr.crt !\n");
        return EXIT_FAILURE;
    }
    else
        printf("\nOPEN FILE dechiffr.crt OK\n");


    //#####
    // INSTRUCTION PRINCIPALE : dechiffrement
    //#####
    int dech;
    int i, j;
    i = j = 0;

    while(i<taille_tab_chiffr)
    {

        dech = tableau_chiffr[i] + pqt[j];
        tableau_dechiffr[i]=dech;
        fwrite(&dech, sizeof(int), 1, fp3);
        i++;
        j++;


        if(j>=strlen(pqt))
           {
                j=0; //remet j a 0 pour recommencer le perroquet
           }

    }



    //FERMETURE FICHIER RESULTAT
    int ret = fclose(fp);
    if (ret != 0)
    {
        printf("\n! FAIL CLOSE FILE dest.crt !\n");
        return EXIT_FAILURE;
    }
    else
        printf("\nCLOSE FILE dest.crt OK\n");


    //COMMENTAIRE ERGONOMIE VISUELLE
    printf("\nBEGIN STEP 6 : CHECKING ENCRYPTION");


    //OUVERTURE SOURCE.txt POUR COMPARAISON
    FILE *fp5=NULL;
    fp5=fopen("source.txt", "rt");
    if (fp5 == NULL)
    {
        printf("\n! WARNING : FAIL OPEN FILE source.txt !\n");
        return EXIT_FAILURE;
    }
    else
        printf("\nOPEN FILE source.txt OK\n");


    // COMPARAISON

    int cpt=0;
    while(cpt<taille_tab_source)
    {

        if(tableau_dechiffr[cpt] != tableau_source[cpt])
        {
            printf("\n! WARNING : erreur, fichiers differents ! ");
        }

        cpt++;
    }

    printf("\nDECRYPTING AND COMPARISON : SUCCESS\n");
    int res=1;

    //FERMETURE
    int ret2 = fclose(fp3);
    if (ret2 != 0)
    {
        printf("\n! WARNING : FAIL CLOSE FILE dechiffr.crt !\n");
        return EXIT_FAILURE;
    }
    else
        printf("\nCLOSE FILE dechiffr.crt OK\n");
return res;
}
